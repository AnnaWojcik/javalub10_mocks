package com.demo.camera;

import java.util.Arrays;


import org.junit.Test;
import org.mockito.Mockito;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.mockingDetails;

public class PhotoCameraTest {


    @Test
    public void turningOnCameraShouldTurnOnSensor() {
        ImageSensor sensor = mock(ImageSensor.class);
        PhotoCamera camera = new PhotoCamera(sensor);

        camera.turnOn();

        Mockito.verify(sensor).turnOn();
    }

    @Test

    public void turningOffCameraShouldTurnOffSensor() {
        ImageSensor sensor = mock(ImageSensor.class);
        PhotoCamera camera = new PhotoCamera(sensor);

        camera.turnOff();

        Mockito.verify(sensor).turnOff();
    }

    @Test
    public void pressingButtonShouldNotWorkWhenCameraIsTurnedOff() {
        ImageSensor sensor = mock(ImageSensor.class);
        PhotoCamera camera = new PhotoCamera(sensor);

        camera.pressButton();

        Mockito.verifyZeroInteractions(sensor);
    }

    @Test
    public void pressingButtonShouldCopyDataFromSensorToCardWhenCameraIsOn() {
        ImageSensor sensor = mock(ImageSensor.class);
        Card card = mock(Card.class);
        PhotoCamera camera = new PhotoCamera(sensor, card);

        camera.turnOn();
        camera.pressButton();

        Mockito.verify(card).write(sensor.read());
    }

    @Test
    public void whileSavingDataTurningOffCameraDoesntWork() {
        ImageSensor sensor = mock(ImageSensor.class);
        Card card = mock(Card.class);
        WriteListener listener = mock(WriteListener.class);
        PhotoCamera camera = new PhotoCamera(sensor, card, listener);

        camera.turnOn();
        camera.pressButton();
        Mockito.clearInvocations(sensor);
        camera.turnOff();

        Mockito.verifyZeroInteractions(sensor);
    }

    @Test
    public void afterSavingDataSensorShouldTurnOff() {
        ImageSensor sensor = mock(ImageSensor.class);
        Card card = mock(Card.class);
        WriteListener listener = mock(WriteListener.class);
        PhotoCamera camera = new PhotoCamera(sensor, card, listener);

        camera.turnOn();
        camera.pressButton();
        camera.writeCompleted();

        Mockito.verify(sensor).turnOff();


    }

}
