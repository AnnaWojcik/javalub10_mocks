package com.demo.camera;

public class PhotoCamera implements WriteListener{

    private ImageSensor sensor;
    private boolean isTurnedOn = false;
    private boolean isDataBeingSaved = false;
    private Card card;
    private WriteListener listener;

    public PhotoCamera(ImageSensor sensor, Card card, WriteListener listener) {
        this.sensor = sensor;
        this.card = card;
        this.listener = listener;
    }

    public PhotoCamera(ImageSensor sensor) {
        this.sensor = sensor;
    }

    public PhotoCamera(ImageSensor sensor, Card card) {
        this.sensor = sensor;
        this.card = card;
    }

    public void turnOn() {
        // not implemented
        sensor.turnOn();
        isTurnedOn = true;
    }

    public void turnOff() {
        // not implemented
        sensor.turnOff();
        isTurnedOn = false;

    }


    public void pressButton() {
        // not implemented
        if(isTurnedOn == true){
            card.write(sensor.read());
        }
    }

    public boolean isTurnedOn() {
        return isTurnedOn;
    }

    @Override
    public void writeCompleted() {
        sensor.turnOff();
    }

    public boolean isDataBeingSaved() {
        return isDataBeingSaved;
    }
}

